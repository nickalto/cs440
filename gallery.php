<?php
$sort_photos = $_POST['photos'];
$sort_videos = $_POST['videos'];
$sort_documents = $_POST['documents'];

$people = $_POST['people']; 
$groups = $_POST['groups']; 
$location = $_POST['location']; 
$search = $_POST['search'];

$pchecked = 0;
$dchecked = 0;
$vchecked = 0;
if($sort_photos != NULL){ $pchecked = 1; }
if($sort_videos != NULL){ $vchecked = 1; }
if($sort_documents != NULL){ $dchecked = 1; }
?>

<html>
	<head>
		<title>OSU Media Search</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<link href="http://oregonstate.edu/~alton/cs440/sitestyle.css" rel="stylesheet" type="text/css" />
	</head>
<body>	
	<article id="grad1">
		<h1>EECS Media Database</h1>
	</article>

<DIV ALIGN=LEFT>
		<form enctype="multipart/form-data" action="gallery.php" method="post"> 	
		<h2><a href="http://oregonstate.edu/~alton/cs440/login.php">Login</a></h2><br>
		<h2><a href="http://oregonstate.edu/~alton/cs440/index.php">Upload</a></h2><br>
			<h2>File Search</h2>
				Photos <input type='checkbox' name="photos" <?php if( $pchecked == 1) {echo "checked";} ?>/>
     			Videos <input type=checkbox name="videos" <?php if( $vchecked == 1) {echo "checked";} ?>/>
				Documents <input type=checkbox name="documents" <?php if( $dchecked == 1) {echo "checked";} ?>> 
				File Info: <input type="text" name="search" value="<?php print $search; ?>">
				People: <input type="text" name="people" value="<?php print $people; ?>"><br> 
				Groups: <input type="text" name = "groups" value="<?php print $groups; ?>"><br> 
				Location: <input type="text" name = "location" value="<?php print $location; ?>"><br><br>					
				<input name="submit" type="submit" class="submit" id="submit" value="search"/>


</DIV>
<div class = "float">
<h2>Download Files</h2>
<?php

// Get information for check boxes as well as text fields

// Search flag used to determine if any of the check boxes are filled
$search_flag = 0;

// Go through each field checking to see if it's checked
// if it is NULL set the search to empty space that the REGEX will not pick up
if($sort_photos != NULL)
{
	$searchi = 'image';
	$search_flag++;
}
else if($sort_photos == NULL)
{
	$searchi = '/////';
}
if($sort_videos != NULL)
{
	$searchv = 'video';
	$search_flag++;
}
else if($search_videos == NULL)
{
	$searchv = '/////';
}
	
if($sort_documents != NULL)
{
	$searchd = 'document';
	$search_flag++;
}
else if($sort_documents == NULL)
{
	$searchd = '/////';
}	

// If all fields are NULL and no check boxes are filled query everything, otherwise query each field using 
// Regular Expressions, if fields are blank the regex's will not pick anything up, for the filetype the regex
// will query all checked boxes, if none are checked it will select everything.
if(($people == NULL) && ($search == NULL) &&  ($location == NULL) && ($groups == NULL) && ($search_flag == 0))
{
	$query = "SELECT f_id, f_name, f_size, data FROM FILE";
}
else
{	
	// If search flag is 0, that means no check boxes are filled, meaning we don't care
	// what type of file we are looking for so search for everything.
	if($search_flag == 0)
	{
		$searchi = ".*";
	}
	if(isset($_POST['submit']))
	{
	
	// Search through file and tag table for corresponding file through regular expressions
			$query = "SELECT FILE.f_id, FILE.f_name, FILE.f_size, FILE.data FROM FILE, TAG WHERE TAG.f_id = FILE.f_id AND TAG.people REGEXP '.*$people.*' AND TAG.groups REGEXP '.*$groups.*' AND TAG.location REGEXP '.*$location.*' AND FILE.f_name REGEXP '.*$search.*' AND (FILE.f_type REGEXP '$searchi' OR FILE.f_type REGEXP '$searchd' OR FILE.f_type REGEXP '$searchv')";
		
	}
}

function formatBytes($size, $precision = 2)
{
    $base = log($size) / log(1024);
    $suffixes = array(' ', ' k', ' MB', ' G', ' T');   

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}
	$dbhost = 'oniddb.cws.oregonstate.edu';
	$dbname = 'alton-db';
	$dbuser = 'alton-db';
	$dbpass = 'nord7J29Ppk3px7e';

	$mysql_handle = mysql_connect($dbhost, $dbuser, $dbpass)or die("Error connecting to database server");  
	mysql_select_db($dbname, $mysql_handle)or die("Error selecting database: $dbname");

	
	$result = mysql_query($query) or die('Error, query failed');

	$files = array();
 
    while ($row = mysql_fetch_array($result)) {
        $f_id = $row['f_id'];
        $files[$f_id] = $row['f_name'];
	$space += $row['f_size'];
    }
echo "Currently using ". formatbytes($space,2) ." of share ON <a href=http://oregonstate.edu/~alton/cs440/phpinfo.php>$dbhost</a>"."<br><br>";
echo "Found ".count($files)." files<br><br>";

?>
 <ul>
                <?php if (count($files) == 0) { ?>
                    <li>No uploaded images found</li>
                <?php } else foreach ($files as $f_id => $f_name) { ?>
                    <li>
                        <a href="http://oregonstate.edu/~alton/cs440/download.php?f_id=<?php echo urlencode($f_id); ?>">
                        	<?php echo htmlSpecialChars($f_name);
				
				?>
                        </a>
                    </li>
                <?

} ?>
</ul>

<?php
mysql_close($mysql_handle);
?>
</form>
</div>
</body>
</html>